func _init(itemstring, stack=1, stack_limit=1, consumable=false, block_damage=0, entity_damage=0, use_speed=0, equipable=false, type=''):
    const itemstring = itemstring
    const on_use = array()
    const stack = stack
    const stack_limit = stack_limit
    const consumable = consumable
    const block_damage = block_damage
    const entity_damage = entity_damage
    const use_speed = use_speed
    const equipable = equipable
func register_on_use(object:FuncRef):
    on_use.append(object)

func on_use(mouse_pos):
    if use_speed > 0:
        # TODO swing
        if consumable:
            stack -= 1
    for i in on_use:
        i.call_func(mouse_pos)
func _Block():
    Block(itemstring, on_use, stack, stack_limit, consumablem block_damage, entity_damage, use_speed, equipable)